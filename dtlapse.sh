#!/bin/sh

# Copyright (C) 2020 Jochen Keil <jochen.keil@gmail.com> and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# check for external ${DTLAPSE_ENVDIR}
if [ "x${DTLAPSE_ENVDIR}x" = "xx" ]; then
  # check for env directory at script location
  DTLAPSE_ENVDIR=$(dirname ${0})/env
  if [ ! -d ${DTLAPSE_ENVDIR} ]; then
    # check for env dir in working directory
    if [ -d {$PWD}/env ]; then
      DTLAPSE_ENVDIR=${PWD}/env
    else
      unset DTLAPSE_ENVDIR
    fi
  fi
fi

# check for external ${DTLAPSE_PY}
if [ "x${DTLAPSE_PY}x" = "xx" ]; then
  # check for dtlapse.py at script location
  DTLAPSE_PY="$(dirname ${0})/dtlapse.py"
  if [ ! -x ${DTLAPSE_PY} ]; then
    DTLAPSE_PY="${PWD}/dtlapse.py"
  fi
  # check for dtlapse.py in working directory
  if [ ! -x ${DTLAPSE_PY} ]; then
    echo "Could not find dtlapse.py!"
    exit 1
  fi
fi

if [ "x${VIRTUAL_ENV}x" = "xx" ]; then
  if [ "x${DTLAPSE_ENVDIR}x" != "xx" ]; then
    if [ -d ${DTLAPSE_ENVDIR} ]; then
      source ${DTLAPSE_ENVDIR}/bin/activate
    fi
  fi
fi

${DTLAPSE_PY} ${@}
