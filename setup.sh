#!/bin/sh

# Copyright (C) 2020 Jochen Keil <jochen.keil@gmail.com> and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

PIP="/usr/bin/env pip3"
PYTHON="/usr/bin/env python3"

ENVDIR=${PWD}/env

if [ "x${1}x" != "xx" ]; then
  ENVDIR=$1
fi

echo "This will setup a virtual environment using python3-venv in\n
    ${ENVDIR}\n
and install the necessary dependencies for running dtlapse.\n
Continue? (y/n)"

read CONTINUE

if [ "x${CONTINUE}x" = 'xyx' ]; then
  if [ -d ${ENVDIR} ]; then
    echo "${ENVDIR} already exists! Quiting.."
    exit 1
  elif [ ! -f "requirements.txt" ]; then
    echo "Could not find requirements.txt! Quiting.."
    exit 1
  else
    ${PYTHON} -m venv ${ENVDIR}
    ${PIP} install -r requirements.txt
  fi
fi
